module Sudoku where
import Data.Char

--Pre defined by assignment
rows = "ABCD"
cols = "1234"
--Defined by exercise 2 task 2
board = cross rows cols

--Pre defined by assignment
containsElem :: Eq a => a -> [a] -> Bool
containsElem _ [] = False
containsElem elem (x:xs)
	| elem == x = True
	| otherwise = containsElem elem xs

-- Cartesian product 
cross :: [a] -> [a] -> [[a]]
-- if empty input return empty
cross [] [] = []
-- For every elemnt in each list, create a pair with every other elelment in the other list
cross (xs) (ys) = [[x,y] | x <- xs, y <- ys]

--Name is self exlpanitory
replacePointsWithZeros :: [Char] -> [Char]
-- if empty input return empty
replacePointsWithZeros [] = []
--Basic if statement that checks if element is "." in the input list of Chars
replacePointsWithZeros (x:xs) = [if (x == '.') then ('0') else x | x <- (x:xs)]

--Take a list for chars and then create a pair for every place on the board conneced to the value from the charlist
parseBoard :: [Char] -> [(String, Int)]
-- if empty input return empty
parseBoard [] = []
-- For every square in board make a pair with the value from the input list
parseBoard (xs) = zip board (map digitToInt (replacePointsWithZeros xs))

peers ::  [(String, [String])]
peers = [(fst list, filterRegList (fst list) (removeDuplicates (foldList (snd list)))) | list <- units]

--all possible rows cols and boxes
unitList :: [[String]]
unitList = [cross [r] cols | r<-rows] ++ [cross rows [c] | c<-cols] ++ [cross (take 2 rows)  (take 2 cols)] ++ [cross (take 2 rows)  (drop 2 cols)] ++ [cross (drop 2 rows)  (take 2 cols)] ++ [cross (drop 2 rows)  (drop 2 cols)]

-- takes out every unit that is connected to the input square
filterUnitList :: String -> [[String]]
-- if empty input return empty
filterUnitList _ = []
--filters the list of all units to retrive the ones containning the inputs quare
filterUnitList x = filter (\x -> (containsElem a x)) unitList

--Help method to remove the units for the square one "stands on" for from the unitlist
filterRegList :: String -> [String] -> [String]
filterRegList _ [] = []
filterRegList x (y:ys)
	| x == y = filterRegList x ys
	| otherwise = y : filterRegList x ys		

-- All rows cols and boxes on the board 
units :: [(String, [[String]])]
units = [(square, filterUnitList square) | square <- board]

-- basic concat
foldList :: [[a]] -> [a]
-- if empty input return empty
foldList [[]] = []
-- basic list concat
foldList xs = concat xs

--makes the list uniq through recurssion
removeDuplicates :: Eq a => [a] -> [a]
removeDuplicates [] = []
--reomves duplicates through reccursion 
removeDuplicates (x:xs) 
	|containsElem x xs = removeDuplicates xs
	|otherwise = x : removeDuplicates xs




